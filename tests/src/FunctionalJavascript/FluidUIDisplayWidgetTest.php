<?php

namespace Drupal\Tests\fluidui\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\node\Entity\Node;

/**
 * Tests if the FluidUI Widget is being displayed correctly.
 *
 * @group fluidui
 */
class FluidUIDisplayWidgetTest extends WebDriverTestBase {

  /**
   * Modules to enable.
   *
   * @var array<string>
   */
  protected static $modules = ['fluidui', 'node'];

  /**
   * Theme to enable.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Disable strict config schema.
   *
   * @var bool
   */
  protected $strictConfigSchema = FALSE;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    // Make sure to complete the normal setup steps first.
    parent::setUp();

    // Log in an administrative user.
    $this->drupalLogin($this->rootUser);
  }

  /**
   * Tests if the widget is displayed in the front page.
   */
  public function testWidgetDisplay() {
    $this->drupalGet('<front>');
    $session = $this->getSession();
    $assert_session = $this->assertSession();
    $page = $session->getPage();

    // Tests if the main Fluid Widget Container is rendered
    // (for older versions of infusion, it's an iframe)
    $fluid_wrapper = $assert_session->elementExists('css', '.fl-prefsEditor-separatedPanel');
    $this->assertTrue($fluid_wrapper->isVisible());

    // Check if the widget buttons were loaded.
    $fluid_buttons = $assert_session->elementExists('css', '.fl-prefsEditor-buttons');
    $this->assertTrue($fluid_buttons->isVisible());

    // Check if the "+show preferences" button exists.
    $show_hide = $assert_session->elementExists('css', '#show-hide');
    $this->assertEquals("+ show preferences", $show_hide->getText());

    $show_preferences_button = $assert_session->elementExists('css', '.fl-prefsEditor-buttons #show-hide');
    $show_preferences_button->click();

    // If the button label is now "hide preferences", it means that
    // the widget was opened successfully.
    $show_hide = $assert_session->elementExists('css', '#show-hide');
    $this->assertEquals("- hide preferences", $show_hide->getText());

    // Check if the sliding panel was rendered.
    $sliding_panel = $assert_session->elementExists('css', '.flc-prefsEditor-separatedPanel .flc-slidingPanel-panel');
    $this->assertTrue($sliding_panel->isVisible());
  }

  /**
   * Creates a page and blacklists it in the admin settings page.
   */
  public function testNotWidgetDisplay() {
    $this->drupalGet('admin/config/fluidui/adminsettings');
    $session = $this->assertSession();

    $this->createContentType(['type' => 'page']);

    Node::create(
          [
            'title' => $this->randomString(),
            'type' => 'page',
            'body' => "Lorem Ipsum",
          ]
      )->save();

    // Add the newly created page in the blacklist.
    $edit = [
      'url_blacklist' => '/node/1',
    ];

    $this->submitForm($edit, 'Save configuration');
    $session->pageTextContains('The configuration options have been saved.');

    $this->drupalGet('/node/1');

    // Tests if the main Fluid Widget Container IS NOT rendered.
    $fluid_wrapper = $session->elementNotExists('css', '.flc-prefsEditor-separatedPanel');
    $this->assertNull($fluid_wrapper);
  }

}
