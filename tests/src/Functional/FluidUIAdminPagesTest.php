<?php

namespace Drupal\Tests\fluidui\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\node\Entity\Node;

/**
 * Tests the FluidUI Admin Pages and the Home Page.
 *
 * @group fluidui
 */
class FluidUIAdminPagesTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array<string>
   */
  protected static $modules = ['fluidui', 'node'];

  /**
   * Theme to enable.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Disable strict config schema.
   *
   * @var bool
   */
  protected $strictConfigSchema = FALSE;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    // Make sure to complete the normal setup steps first.
    parent::setUp();

    // Log in an administrative user.
    $this->drupalLogin($this->rootUser);
  }

  /**
   * Tests if user can load the front page among other things.
   */
  public function testLoadSite() {
    // Load the front page.
    $this->drupalGet('<front>');

    // Confirm that the site didn't throw a server error or something else.
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Tests the admin page.
   */
  public function testAdminPage() {
    $this->drupalGet('admin/config/fluidui/adminsettings');

    $session = $this->assertSession();
    $session->statusCodeEquals(200);
    $session->pageTextContains('Enter the list of pages where the toolbox will not be displayed. Specify pages by using their paths. Enter one path per line.');
    $session->pageTextContains('Hide the toolbox on these pages');
    $session->pageTextContains('Display preferences toolbox on admin pages');

    $this->createContentType(['type' => 'page']);

    Node::create(
          [
            'title' => $this->randomString(),
            'type' => 'page',
            'body' => "Loremp",
          ]
      )->save();

    // Add the newly created page in the blacklist.
    $edit = [
      'url_blacklist' => '/node/1',
    ];

    $this->submitForm($edit, 'Save configuration');
    $session->statusCodeEquals(200);
    $session->pageTextContains('The configuration options have been saved.');
  }

}
