# Fluid UI - Infusion

This module integrates the fluidproject.org UI Options accessibility framework,
which allows visitors to customize site appearance, including font size, line
height, site contrast, generate a table of contents from the `<h>` tags, and
underlining and bolding links.

The module adds a "+ show display preferences" tab to the top right of the page,
which toggles the UI Options. The framework uses cookies to save user
preferences.

The module includes a precompiled framework JS file from the source but you can
compile a new file using the [source code](https://github.com/fluid-project/infusion)

**Important Note**: You may need to uninstall quick_edit module.
This module causes FluidUI to be shown twice when logged in with admin bar

For a full description of the module, visit the
[project page](https://www.drupal.org/project/fluidui).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/fluidui).


## Table of contents

- Requirements
- Installation
- Configuration
- Customization


## Requirements

This module requires the following library:

[FluidUI Infusion Library](https://github.com/fluid-project/infusion) 


## Installation

- Download the [source code](https://github.com/fluid-project/infusion) 

- From the project root directory, run:

  `npm install`

  For installing/updating the project dependencies

- Also from the project root directory, run:

  `grunt custom --exclude="jQuery" --include="uiOptions"`

  For building the uiOptions package from the source files

  - On the "/libraries" directory from your site's document root, create the "infusion" directory.

  - Copy "build/src/" from the project root directory to your Drupal /libraries/infusion directory.

  - Copy the file infusion-custom.js from the build/ project root directory to your Drupal
    _/libraries/infusion/_ directory.

Then install the module as usual with composer:

`composer require 'drupal/fluidui:^2.0'`


## Configuration

You can control the list of pages where the toolbox will not be displayed and also if you
want the toolbox to be displayed on admin pages (such as /admin/content, /admin/structure, etc.).
The configuration form is found on _/admin/config/fluidui/adminsettings_


## Customization

- Style customization
  Use the module's css/fluid.css file to customize the appearance and placement
  of the "+ show display preferences" tab.

- Multilingual support
  The framework supports multilingual labels and text, but it must be created
  manually and cannot be created from the Drupal admin UI. 

  Follow these instructions and replace "fr" with the language code of your choice:

  1. Copy /messages/en to /messages/fr and translate the contents of variables in the files to your language
  2. Copy /toc/en to /toc/fr and translate TableOfContents.html
  3. Edit /js/fluidui_load.js according to the following example code:

  var langCode = drupalSettings.path.currentLanguage;

  ```
  if (langCode == "en") {
    fluid.uiOptions.prefsEditor(".flc-prefsEditor-separatedPanel", {
      tocTemplate: modulePath + "/infusion/src/components/tableOfContents/html/TableOfContents.html",
      terms: {
        templatePrefix: modulePath + "/infusion/src/framework/preferences/html",
        messagePrefix: modulePath + "/messages/en"
      }
    });
  } else if (langCode == "fr") {
    fluid.uiOptions.prefsEditor(".flc-prefsEditor-separatedPanel", {
      tocTemplate: modulePath + "/toc/fr/TableOfContents.html",
      terms: {
        templatePrefix: modulePath + "/infusion/src/framework/preferences/html",
        messagePrefix: modulePath + "/messages/fr"
      }
    });
  }
  ```
