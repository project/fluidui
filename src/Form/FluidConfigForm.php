<?php

namespace Drupal\fluidui\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Config settings form for the Fluid module.
 */
class FluidConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'fluidui.adminsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fluidui_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('fluidui.adminsettings');

    $form['admin_display'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display preferences toolbox on admin pages'),
      '#description' => $this->t('Check this option if you want the toolbox to be displayed on admin pages (such as /admin/content, /admin/structure, etc.)'),
      '#default_value' => $config->get('admin_display'),
    ];

    $form['url_blacklist'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Hide the toolbox on these pages'),
      '#description' => $this->t('Enter the list of pages where the toolbox will not be displayed. Specify pages by using their paths. Enter one path per line.'),
      '#default_value' => $config->get('url_blacklist'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('fluidui.adminsettings')
      ->set('admin_display', $form_state->getValue('admin_display'))
      ->save();

    $this->config('fluidui.adminsettings')
      ->set('url_blacklist', $form_state->getValue('url_blacklist'))
      ->save();
  }

}
