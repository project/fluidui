(function ($, Drupal) {
  Drupal.behaviors.fluid = {
    attach: (context, settings) => {
      if (!$('.fl-prefsEditor-separatedPanel-iframe').length) {
        const modulePath = drupalSettings.modulePath;

        const langCode = drupalSettings.path.currentLanguage;

        /* no-undef */
        fluid.uiOptions.prefsEditor('.flc-prefsEditor-separatedPanel', {
          tocTemplate: modulePath + '/infusion/src/components/tableOfContents/html/TableOfContents.html',
          terms: {
            templatePrefix: modulePath + '/infusion/src/framework/preferences/html',
            messagePrefix: modulePath + '/infusion/src/framework/preferences/messages'
          },
        });

        const iframeEleme = document.getElementsByClassName("fl-prefsEditor-separatedPanel-iframe");

        iframeEleme[0].setAttribute('title', 'Iframe for the Web Interface Settings')

        $('.fl-prefsEditor-buttons #show-hide').keypress(function (e) {
          function setFocusThickboxIframe() {
            const iframeRef = document.getElementsByClassName('fl-prefsEditor-separatedPanel-iframe');
            const iframe = $(iframeRef)[0];
            const iframewindow = iframe.contentWindow;

            iframewindow.focus();
          }

          if (navigator.userAgent.indexOf('Firefox') !== -1) {
            setTimeout(setFocusThickboxIframe, 100);
          } else {
            setTimeout(setFocusThickboxIframe, 100);
          }
        });
      }
    },
  };
})(jQuery, Drupal);
